﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    public float moveSpeed;
    public bool moveRight;
    private Rigidbody2D rigi;

    public Transform wallCheck;
    public float wallCheckRadius;
    public LayerMask whatIsWall;
    private bool hittingWall;
    private bool notAtEdge;
    public Transform edgeCheck;


    void Start()
    {
        rigi = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        hittingWall = Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, whatIsWall);
        notAtEdge = Physics2D.OverlapCircle(edgeCheck.position, wallCheckRadius, whatIsWall);

        if (hittingWall || ! notAtEdge) {
            moveRight = !moveRight;
        }
        if (moveRight)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            rigi.velocity = new Vector2(moveSpeed, rigi.velocity.y);
        }
        else
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
            rigi.velocity = new Vector2(-moveSpeed, rigi.velocity.y);
        }
        
    }
}
