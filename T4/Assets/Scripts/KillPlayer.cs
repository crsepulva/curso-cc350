﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayer : MonoBehaviour
{
    [SerializeField] Transform spawnPoint;
    private PlayerMovement play;
    private  Collider2D capsule;
    private Health health;
    void Start()
    {
        capsule = GameObject.FindGameObjectWithTag("Player").GetComponent<CapsuleCollider2D>();

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other == capsule) {
        if (other.transform.CompareTag("Player"))
        {
            other.transform.position = spawnPoint.position;
            play = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
            health = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
            health.health--;

       
            play.TriggerHurt(3);



        }}


        
    }
}
