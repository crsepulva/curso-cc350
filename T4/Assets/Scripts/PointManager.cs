﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class PointManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static PointManager instance;
    public TextMeshProUGUI text;
    private int score;
    void Start()
    {

        if (instance == null)
        {
            instance = this;

        }

    }

    public void ChangeScore(int coinValue)
    {
        score += coinValue;
        PlayerPrefs.SetInt("Points", score);
        text.text = "points " + score.ToString();

    }
}
