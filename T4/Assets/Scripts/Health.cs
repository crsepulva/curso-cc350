﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Health : MonoBehaviour
{
    //Variables
    public int health;
    public int numbOfHearts;
    public int numbOfLives;

    public Image[] hearts;
    public Sprite fullHeart;
    public Sprite emptyHeart;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()

    {
        if (health > numbOfHearts)
        {
            health = numbOfHearts;
        }
        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < health)
            {
                hearts[i].sprite = fullHeart;

            }
            else
            {
                hearts[i].sprite = emptyHeart;
            }

            if (i < numbOfHearts)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }

            if (health <= 0)
            {
                Die();
            }
        }
    }
    void Die()
    {
        if (numbOfLives >= 0)
        {
            numbOfLives--;
            LiveManager.instance.changeLives(numbOfLives);
            Debug.Log(numbOfLives);
            health = numbOfHearts;
        }
        if (numbOfLives < 0)
        {

            SceneManager.LoadScene(3);
        }
       
    }
}
