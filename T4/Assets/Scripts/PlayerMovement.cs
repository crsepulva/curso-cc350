﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public CharacterController2D controller;
    float horizontalMove = 0f;
    public float runSpeed = 40f;
    bool jump = false;
    bool crouch = false;
    //bool hurt = false;
    public Animator animator;
    public float knockback;
    public float knockbackCount;
    public float knockbackLenght;
    public float hurtTime;
    private Rigidbody2D body;
    Collider2D[] myColls;
    private Health health;


    public bool knockFromRight;

    void Start()
    {
        myColls = this.GetComponents <Collider2D>();
        body = GetComponent<Rigidbody2D>();
        health = GetComponent<Health>();

    }

    // Update is called once per frame
    void Update()

    {
        if (knockbackCount <= 0)
        {

            horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
            animator.SetFloat("Speed", Mathf.Abs(horizontalMove));
            if (Input.GetButtonDown("Jump"))
            {
                jump = true;
                animator.SetBool("IsJumping", true);
            }
            if (Input.GetButtonDown("Crouch"))
            {
                crouch = true;
            }
            else if (Input.GetButtonUp("Crouch"))
            {
                crouch = false;
            }
        }
        else
        {
            if (knockFromRight)
            {
                //transform.localRotation = Quaternion.Euler(0, 0, 0);
                animator.SetBool("IsHurt", true);
                body.velocity = new Vector2(-knockback, knockback);
                //transform.localRotation = Quaternion.Euler(0, 0, 0);
                TriggerHurt(hurtTime);
            }
            if (!knockFromRight)
            {
                //transform.localRotation = Quaternion.Euler(0, 0, 0);
                animator.SetBool("IsHurt", true);
                body.velocity = new Vector2(knockback, knockback);
                //transform.localRotation = Quaternion.Euler(0, 0, 0);
                TriggerHurt(hurtTime);
            }
            knockbackCount -= Time.deltaTime;
        }


    }

    public void OnLanding()
    {
        animator.SetBool("IsJumping", false);
        animator.SetBool("IsHurt", false);
    }

    public void OnCrouching(bool isCrouching)
    {
        animator.SetBool("IsCrouching", isCrouching);
    }
    private void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;

    }
    public void TriggerHurt(float hurtTime)
    {
        StartCoroutine(HurtBlinker(hurtTime));


    }

    IEnumerator HurtBlinker(float hurtTime)
    {
        int enemyLayer = LayerMask.NameToLayer("Enemy");
        int playerLayer = LayerMask.NameToLayer("Player");
        Physics2D.IgnoreLayerCollision(enemyLayer, playerLayer);
        foreach (Collider2D collider in myColls)
        {
            collider.enabled = false;
            collider.enabled = true;
        }


        //Start looping blinkg anim
        animator.SetLayerWeight(1, 1);

        //wait
        yield return new WaitForSeconds(hurtTime);

        //Stop blinking and re enable collision
        Physics2D.IgnoreLayerCollision(enemyLayer, playerLayer, false);
        animator.SetLayerWeight(1, 0);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Coin"))
        {
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("MovingPlatform"))
        {
            transform.parent = collision.transform;


            if (collision.gameObject.CompareTag("Hearth"))
            {
                Destroy(collision.gameObject);
            }

        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "MovingPlatform")
        {
            transform.parent = other.transform;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.transform.tag == "MovingPlatform")
        {
            transform.parent = null;
        }
    }
}




