﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public int coinValue = 1;
    private Collider2D capsule;


    void Start()
    {
        capsule = GameObject.FindGameObjectWithTag("Player").GetComponent<CapsuleCollider2D>();
 }
    private void OnTriggerEnter2D(Collider2D other)
    {
        capsule = other.gameObject.GetComponent<CapsuleCollider2D>();
        if (other.gameObject.CompareTag("Player") && other==capsule )
        {
            ScoreManager.instance.ChangeScore(coinValue);
            PointManager.instance.ChangeScore(10);
        }
    }
}
