﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHurtPlayer : MonoBehaviour
{
    // Start is called before the first frame update
    private Collider2D capsule;
    private Health health;
    private Transform target;
    PlayerMovement player;
    public int damage;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        capsule = GameObject.FindGameObjectWithTag("Player").GetComponent<CapsuleCollider2D>();
        health = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
        



    }

    // Update is called once per frame
    void Update()
    {

    }
    void Hurt(int damage,Health health)
    {
        Debug.Log("giving " + damage + "to" + health.health);
        health.health -= damage;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="Player" && collision==capsule)
        {
           var player = collision.GetComponent<PlayerMovement>();
            Hurt(damage, health);

            player.knockbackCount = player.knockbackLenght;
            if (collision.transform.position.x < transform.position.x)
            {
                player.knockFromRight = true;
            }
            else
            {
                player.knockFromRight = false;
            }

        
    

        }
      
    }
}

            

