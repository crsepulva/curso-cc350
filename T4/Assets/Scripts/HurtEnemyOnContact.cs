﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtEnemyOnContact : MonoBehaviour
{
    public int damageToGive;
    public float bounceOnEnemy;
    private Rigidbody2D myrigidbody2D;
    private Animator animator;
    
    
    void Start()
    {
        myrigidbody2D = transform.parent.GetComponent<Rigidbody2D>();
        animator = transform.parent.GetComponent<Animator>();

        
       


        
    }

    // Update is called once per frame
    void Update()
    {
        
    }




    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Enemy"))
        {

         
            animator.SetBool("IsJumping", true);
            Destroy(collision.transform.root.gameObject);
            myrigidbody2D.velocity = new Vector2(myrigidbody2D.velocity.x, bounceOnEnemy);
            PointManager.instance.ChangeScore(20);

        }
    }
   
    


   

}

