﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LiveManager : MonoBehaviour
{   // Start is called before the first frame update
    public static LiveManager instance;
    public TextMeshProUGUI text;
    private int lives;
    void Start()
    {
        if (instance == null)
        {
            instance = this;

        }
    }

    public void changeLives(int liveValue)
    {
        lives = liveValue;
        text.text = "x " + lives.ToString();

    }
}
