﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChase : MonoBehaviour
{
    public float speed;
    private Transform target;
    private Collider2D capsule;
    private Health health;
    //public GameObject Enemy;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        capsule = GameObject.FindGameObjectWithTag("Player").GetComponent<CapsuleCollider2D>();


    }

    // Update is called once per frame
    void Update()
    {
        if (Vector2.Distance(transform.position, target.position) > 1.5)
        {

            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other == capsule)
        {
            {
                if (other.transform.CompareTag("Player"))
                {
                    health = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();

                }

            }



        }


    }
}
