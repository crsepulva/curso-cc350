import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import sys
import math
import csv


import threading




#from ex_obj_reader import *
import lib.basic_shapes_extended as bs_ext
import lib.catrom as catrom

import lib.transformations as tr
import lib.basic_shapes as bs
import lib.scene_graph as sg
import lib.easy_shaders as es
import lib.lighting_shaders as ls
import time


PROJECTION_ORTHOGRAPHIC = 0
PROJECTION_FRUSTUM = 1
PROJECTION_PERSPECTIVE = 2
# A class to store the application control
class Controller:
    def __init__(self):
        self.fillPolygon = True
        self.showAxis = False
        self.follow_car = False
        self.lights = False
        self.ChangeView = False
        self.projection = PROJECTION_PERSPECTIVE
        self.POSCAM = 0
        self.estatica = False
        self.record = False
        

    firstMouse = True
    cameraPos   = np.array([4.91995052, 4.9419168, 2.48825306])
    cameraFront = np.array([-0.71040869, -0.70300799, -0.03315518])
    cameraUp    = np.array([0.0, 0.0,  1.0])
    yaw   = -135.28   # yaw is initialized to -90.0 degrees since a 
                            # yaw of 0.0 results in a direction 
                            # vector pointing to the right so we initially rotate a bit to the left.
    pitch =  -1.9
    lastX =  1649.0
    lastY =  195.0
    fov   =  4000.0
    move_sides = 0.0
    move_updown = 0.0

# We will use the global controller as communication with the callback function
controller = Controller()

def mouse_callback(window, xpos, ypos):
    global controller
    if (controller.firstMouse):
        controller.lastX = xpos
        controller.lastY = ypos
        controller.firstMouse = False
    '''
    print('primerx: ' + str(controller.lastX))
    print('primery: ' + str(controller.lastY))
    '''

    xoffset = -xpos + controller.lastX # reversed since x-coordinates go from bottom to top
    yoffset = controller.lastY - ypos # reversed since y-coordinates go from bottom to top
    controller.lastX = xpos
    controller.lastY = ypos

    sensitivity = 0.05 # change this value to your liking
    xoffset *= sensitivity
    yoffset *= sensitivity

    controller.yaw += xoffset
    controller.pitch += yoffset

    #make sure that when controller.pitch is out of bounds, screen doesn't get flipped
    if (controller.pitch > 89.0):
        controller.pitch = 89.0
    if (controller.pitch < -89.0):
        controller.pitch = -89.0

    ex = np.cos(math.radians(controller.yaw)) * np.cos(math.radians(controller.pitch))
    ey = np.sin(math.radians(controller.pitch))
    ez = np.sin(math.radians(controller.yaw)) * np.cos(math.radians(controller.pitch))

    ex = np.cos(math.radians(controller.yaw)) * np.cos(math.radians(controller.pitch))
    ey = np.sin(math.radians(controller.yaw)) * np.cos(math.radians(controller.pitch))
    ez = np.sin(math.radians(controller.pitch))
    front=np.array([ex,ey,ez])
    #print(np.cos(math.radians(controller.yaw)) * np.cos(math.radians(controller.pitch)))
    controller.cameraFront = front / np.linalg.norm(front)

def scroll_callback(window, xoffset, yoffset):
    #print(yoffset)
    if (controller.fov >= -20.0 and controller.fov <= 10000.0):
        controller.fov -= yoffset*10
        #print(controller.fov)
    if (controller.fov < -20.0):
        controller.fov = -20.0
    if (controller.fov > 10000.0):
        controller.fov = 10000.0

def changeView(n):
    
    if n==1:
        eye = np.array([ 0.14544578 ,-8.7404129 ,  4.91253141])
        at = np.array([ 0.01903554 , 0.96505721, -0.26134694])
        up = np.array([0., 0., 1.])
        normal_view = tr.lookAt(eye,at,up)
    elif n==2:
        eye = np.array([ 9.89438631, -0.11091709 , 2.61833856])
        at = np.array([-0.99299852 , 0.0117857 ,  0.1175374 ])
        up = np.array([0., 0., 1.])
        normal_view = tr.lookAt(eye,at,up)

    elif n==3:
        eye = np.array([-0.29059818 , 9.24681088 , 5.07846319])
        at = np.array([-0.00134932 ,-0.96637514 ,-0.25713279])
        up = np.array([0.0, 0.0,  1.0])
        normal_view = tr.lookAt(eye,at,up)
    elif n==4:  
        eye = np.array([-8.4970347 , -0.03211853 , 5.85733488])
        at = np.array([ 0.9494213 ,  0.002817   ,-0.31399246])
        up = np.array([0.0, 0.0,  1.0])
        normal_view = tr.lookAt(eye,at,up)

    elif n==5:  
        eye = np.array([10.04164632, 13.99480324, 14.19807597])
        at = np.array([-0.03541168, -0.69186712, -0.72115594])
        up = np.array([0., 0., 1.])
        normal_view = tr.lookAt(eye,at,up)

    return normal_view

def on_key(window, key, scancode, action, mods):

    if action != glfw.PRESS:
        return
    
    global controller

    if key == glfw.KEY_SPACE:
        controller.fillPolygon = not controller.fillPolygon

    #elif key == glfw.KEY_7:
        #print('Orthographic projection')
        #controller.projection = PROJECTION_ORTHOGRAPHIC

    #elif key == glfw.KEY_8:
        #print('Frustum projection')
        #controller.projection = PROJECTION_FRUSTUM

    elif key == glfw.KEY_9:
        print('Perspective projection')
        controller.projection = PROJECTION_PERSPECTIVE

    elif key == glfw.KEY_RIGHT_CONTROL:
        print('reset ubication')
        controller.ChangeView = False

    elif key == glfw.KEY_G:
        print('Ground Changed')
        controller.changeGround = not controller.changeGround

    elif key == glfw.KEY_1:
        print('Camera 1 Localized')
        controller.POSCAM = 1
        controller.ChangeView = True
        

    elif key == glfw.KEY_2:
        print('Camera 2 Localized')
        controller.POSCAM = 2
        controller.ChangeView = True
        

    elif key == glfw.KEY_3:
        print('Camera 3 Localized')
        controller.POSCAM = 3
        controller.ChangeView = True
        

    elif key == glfw.KEY_4:
        print('Camera 4 Localized')
        controller.POSCAM = 4
        controller.ChangeView = True

    elif key == glfw.KEY_ENTER:
        print('IM YOUR FATHER')
        controller.record = not controller.record
      

    elif key == glfw.KEY_ESCAPE:
        sys.exit()


def on_key_with_arrows(window, key, scancode, action, mods):
    if action != glfw.PRESS:
        return

    global controller

    if key == glfw.KEY_SPACE:
        controller.fillPolygon = not controller.fillPolygon

    elif key == glfw.KEY_1:
        print('Orthographic projection')
        controller.projection = PROJECTION_ORTHOGRAPHIC

    elif key == glfw.KEY_2:
        print('Frustum projection')
        controller.projection = PROJECTION_FRUSTUM

    elif key == glfw.KEY_3:
        print('Perspective projection')
        controller.projection = PROJECTION_PERSPECTIVE

    elif key == glfw.KEY_RIGHT:
        print('KEY_RIGHT')

    elif key == glfw.KEY_LEFT:
        print('KEY_RIGHT')

    elif key == glfw.KEY_UP:
        print('KEY_RIGHT')

    elif key == glfw.KEY_DOWN:
        print('KEY_RIGHT')

    elif key == glfw.KEY_ESCAPE:
        sys.exit()


def createScene(file):  
    gpuTextureCube = es.toGPUShape(bs.createTextureCube(file), GL_REPEAT, GL_NEAREST)
    #gpuTextureCube = es.toGPUShape(bs.createColorCube(152/277,75/277,31/277), GL_REPEAT, GL_NEAREST)


    cielo_scaled = sg.SceneGraphNode("cielo_scaled")
    cielo_scaled.transform = tr.scale(50, 50, 30)
    cielo_scaled.childs += [gpuTextureCube]

    cielo_rotated = sg.SceneGraphNode("cielo_rotated_x")
    cielo_rotated.transform = tr.rotationX(0)
    cielo_rotated.childs += [cielo_scaled]

    cielo = sg.SceneGraphNode("cielo")
    cielo.transform = tr.translate(0, 0, 14.)
    cielo.childs += [cielo_rotated]

    return cielo
def createStructure(n,m):
    gpuTextureCube = es.toGPUShape(bs.createTextureCube("image/ruins.png"), GL_REPEAT, GL_NEAREST)

    cube_base=sg.SceneGraphNode("Cube_Base")
    cube_base.childs+=[gpuTextureCube]
    list=[]
    for i in range(n):
        for j in range(m):
            nom = 'base_' + str(i) + str(j)
            base = sg.SceneGraphNode(nom)
            base.transform = tr.translate(-5 + 1*i,-1 + 1*j, 0)
            base.childs += [cube_base]
            list.append(base)
    baseNM=sg.SceneGraphNode("suelo"+str(n)+str(m))
    for i in range(len(list)):
        baseNM.childs += [list[i]]  
    return baseNM   
def createladder():
    line = createStructure(8,1)
    ladderbase=sg.SceneGraphNode("ladderbase")
    ladderbase.transform=tr.translate(2,14.5,-0.8)
    ladderbase.childs +=[line]

    ladderszd = sg.SceneGraphNode("ladderresized")
    ladderszd.transform=tr.scale(0.8,0.8,0.8)
    ladderszd.childs += [ladderbase]
    return ladderszd
def createEsferas():
    gpuPrisma = es.toGPUShape(bs.createSphereTexture("image/ruins.png",1), GL_REPEAT, GL_NEAREST)

    prisma_scaled = sg.SceneGraphNode("prisma_scaled")
    prisma_scaled.transform = tr.scale(2, 2, 2)
    prisma_scaled.childs += [gpuPrisma]

    prisma_rotated = sg.SceneGraphNode("prisma_rotated_x")
    prisma_rotated.transform = tr.rotationX(0)
    prisma_rotated.childs += [prisma_scaled]

    prisma = sg.SceneGraphNode("prisma")
    prisma.transform = tr.translate(8, 5, 5.5)
    prisma.childs += [prisma_rotated]

    prisma2 = sg.SceneGraphNode("prisma2")
    prisma2.transform = tr.translate(-8, 5,5.5)
    prisma2.childs +=[prisma_rotated]

    prisma3 = sg.SceneGraphNode("prisma3")
    prisma3.transform =  tr.scale(1.5,1.5,1.5)
    prisma3.childs += [prisma_rotated]

    prisma3tr= sg.SceneGraphNode("prisma3tr")
    prisma3tr.transform = tr.translate(0,0,6.5)
    prisma3tr.childs +=[prisma3]

    fullsphere = sg.SceneGraphNode("full")
    fullsphere.childs += [prisma3tr]
    fullsphere.childs += [prisma2]
    fullsphere.childs += [prisma]


    return fullsphere







def createCube(n):
    #gpuTextureCube = es.toGPUShape(bs.createTextureCube("tower_noir_windows.jpg"), GL_REPEAT, GL_NEAREST)
    gpuTextureCube = es.toGPUShape(bs.createTextureCube("image/towe_black.jpg"), GL_REPEAT, GL_NEAREST)
    gpuTextureWin = es.toGPUShape(bs.createTextureCube("image/towe_black.jpg"), GL_REPEAT, GL_NEAREST)

    #--------------------
    #tower base
    cube_scaled = sg.SceneGraphNode("cube_scaled")
    cube_scaled.transform = tr.scale(.7, .7, 1)
    cube_scaled.childs += [gpuTextureCube]

    cube_rotated = sg.SceneGraphNode("cube_rotated_x")
    cube_rotated.transform = tr.rotationX(0)
    cube_rotated.childs += [cube_scaled]

    cube_tower = sg.SceneGraphNode("cube_tower")
    cube_tower.transform = tr.translate(0, 0, .5)
    cube_tower.childs += [cube_rotated]

    #--------------------
    #tower with windows
    pisos = []
    for i in range(n):
        nom_scale = "win_scaled_" + str(i)
        win_scaled = sg.SceneGraphNode(nom_scale)
        win_scaled.transform = tr.scale(.7, .7, .25)
        win_scaled.childs += [gpuTextureWin]

        nom_rotated = "win_rotated_" + str(i)
        win_rotated = sg.SceneGraphNode(nom_rotated)
        win_rotated.transform = tr.rotationX(0)
        win_rotated.childs += [win_scaled]

        nom_trans = "win_trans_" + str(i)
        win_trans = sg.SceneGraphNode(nom_trans)
        win_trans.transform = tr.translate(0,0,1.1 + i*0.25)
        win_trans.childs += [win_scaled]
        pisos.append(win_trans)

    cube = sg.SceneGraphNode("cube")
    cube.transform = tr.translate(0,0,0)
    cube.childs += [cube_tower]
    for i in range(len(pisos)):
        cube.childs += [pisos[i]]
    return cube

def createSide():
    base = createStructure(8,10)
    side_base=sg.SceneGraphNode("Base")
    side_base.childs+=[base]

    base_translated = sg.SceneGraphNode("Base_translated")
    base_translated.transform = tr.translate(0, 0, 4)
    base_translated.childs += [base]

    structure = sg.SceneGraphNode("Base_translated")
    structure.childs += [base_translated]
    structure.childs += [base]



    return structure

def createleftpilars(n,m):
    cilinder = createCylinder()
    cil_base=sg.SceneGraphNode("Cilinder_Base")
    cil_base.childs+=[cilinder]
    list=[]
    for i in range(n):
        for j in range(m):
            nom = 'pilar_' + str(i) + str(j)
            pilar = sg.SceneGraphNode(nom)
            pilar.transform = tr.translate(-5+ 3*i,-1 + 3*j, 0)
            pilar.childs += [cil_base]
            list.append(pilar)
    pilares=sg.SceneGraphNode("pilares"+str(n)+str(m))
    for i in range(len(list)):
        pilares.childs += [list[i]]  
    return pilares     

def createrightPilars(n,m):
     pilars= createleftpilars(n,m)
     rightpilars_base=sg.SceneGraphNode("Right_PilarsBase")
     rightpilars_base.transform=tr.translate(+15.5,0,0)
     rightpilars_base.childs += [pilars]

     rightpilars_tr=sg.SceneGraphNode("Right_PilarsBaseTrans")
     rightpilars_tr.transform=tr.translate(0.5,0,0)
     rightpilars_tr.childs += [rightpilars_base]



     return rightpilars_tr
def createrightSide():
     base=createSide()
     rightside_base=sg.SceneGraphNode("Right_PilarsBase")
     rightside_base.transform=tr.translate(+15,0,0)
     rightside_base.childs += [base]

     return rightside_base
def createCenterSide():
    base = createStructure(8,12)
    side_base=sg.SceneGraphNode("Base")
    side_base.childs+=[base]

    base_translated = sg.SceneGraphNode("Base_translated")
    base_translated.transform = tr.translate(0, 0, 4)
    base_translated.childs += [base]

    structure = sg.SceneGraphNode("Base_translated")
    structure.childs += [base_translated]
    structure.childs += [base]

    structure2 = sg.SceneGraphNode("Base_translated")
    structure2.transform = tr.translate(2, 0,0 )
    structure2.childs +=[structure]
    
    return structure2

def createcenterpilars(n,m):
    cilinder = createCylinder()
    cil_base=sg.SceneGraphNode("Cilinder_Base")
    cil_base.childs+=[cilinder]
    list=[]
    for i in range(n):
        for j in range(m):
            nom = 'pilar_' + str(i) + str(j)
            pilar = sg.SceneGraphNode(nom)
            pilar.transform = tr.translate(-1.5+ 3*i,-1 + 3*j, 0)
            pilar.childs += [cil_base]
            list.append(pilar)
    pilares=sg.SceneGraphNode("pilares"+str(n)+str(m))
    for i in range(len(list)):
        pilares.childs += [list[i]]  
    return pilares


def createSideTemple():
    leftbasenode =sg.SceneGraphNode("leftbase")
    leftbase=createSide()
    leftbasenode.childs +=[leftbase]
    rightbasenode = sg.SceneGraphNode("righbase")
    rightbase=createrightSide()
    rightbasenode.childs +=[rightbase]

    fullbase = sg.SceneGraphNode("base")
    fullbase.transform = tr.translate(-5,0,0)
    fullbase.childs +=[leftbasenode]
    fullbase.childs +=[rightbasenode]
    return fullbase

def createsidepilars():
    rightpilarnode =sg.SceneGraphNode("rightpilars")
    rightpilars = createrightPilars(3,4)
    rightpilarnode.childs += [rightpilars]
    leftpilarnode =sg.SceneGraphNode("lefttpilars")
    leftpilars=createleftpilars(3,4)
    leftpilarnode.childs += [leftpilars]

    pilars = sg.SceneGraphNode("pilars")
    pilars.transform = tr.translate(-5,0,0)
    pilars.childs += [rightpilarnode]
    pilars.childs += [leftpilarnode]

    return pilars 

def createTop():
    top = es.toGPUShape(bs.createColorTriangularPrism("image/ruins.png"), GL_REPEAT, GL_NEAREST)
    prisma_scaled = sg.SceneGraphNode("prisma_scaled")
    prisma_scaled.transform = tr.scale(2, 2, 2)
    prisma_scaled.childs += [top]

    return prisma_scaled





    
    

def createScenario(file):
    gpuGround_texture = es.toGPUShape(bs.createTextureQuad(file), GL_REPEAT, GL_NEAREST)
    ground_scaled = sg.SceneGraphNode("ground_scaled")
    ground_scaled.transform = tr.scale(50, 50, 10)
    ground_scaled.childs += [gpuGround_texture]

    ground_rotated = sg.SceneGraphNode("ground_rotated_x")
    ground_rotated.transform = tr.rotationX(0)
    ground_rotated.childs += [ground_scaled]

    ground = sg.SceneGraphNode("ground")
    ground.transform = tr.translate(0, 0, -0.9)
    ground.childs += [ground_rotated]

    gpuSky_texture = es.toGPUShape(bs.createTextureQuad("image/stars.png"), GL_REPEAT, GL_NEAREST)

    sky_scaled = sg.SceneGraphNode("sky_scaled")
    sky_scaled.transform = tr.scale(50, 50, 10)
    sky_scaled.childs += [gpuSky_texture]

    sky = sg.SceneGraphNode("sky")
    sky.transform = tr.translate(0, 0, 25.)
    sky.childs += [sky_scaled]

    scenario = sg.SceneGraphNode("scenario")
    scenario.childs += [sky]
    scenario.childs += [ground]

    return scenario

def createcubito(file):
    gpuGround_texture = es.toGPUShape(bs.createTextureQuad(file), GL_REPEAT, GL_LINEAR)
    ground_scaled = sg.SceneGraphNode("ground_scaled")
    ground_scaled.transform = tr.scale(10,10, 10)
    ground_scaled.childs += [gpuGround_texture]

    ground_rotated = sg.SceneGraphNode("ground_rotated_x")
    ground_rotated.transform = tr.rotationX(0)
    ground_rotated.childs += [ground_scaled]

    ground = sg.SceneGraphNode("ground")
    ground.transform = tr.translate(0, 0, -2)
    ground.childs += [ground_rotated]

    return ground
# Create ground with textures
def createGround(file):
    gpuGround_texture = es.toGPUShape(bs.createTextureQuad(file), GL_REPEAT, GL_NEAREST)

    ground_scaled = sg.SceneGraphNode("ground_scaled")
    ground_scaled.transform = tr.scale(2, 2, 2)
    ground_scaled.childs += [gpuGround_texture]

    ground_tras = sg.SceneGraphNode("ground_tras")
    ground_tras.transform = tr.translate(-2.5, -2.5, -0.8)
    ground_tras.childs += [ground_scaled]

    lista = []

    for i in range(16):
        for j in range(16):
            nom = 'ground_' + str(i) + str(j)
            ground = sg.SceneGraphNode("ground")
            ground.transform = tr.translate(-1 + 1*i,-1 + 1*j, 0)
            ground.childs += [ground_tras]
            lista.append(ground)
    
    suelo = sg.SceneGraphNode("ground")
    for i in range(len(lista)):
        suelo.childs += [lista[i]]

    return suelo    

def createCylinder():
    color = [209/277,135/277,88/277]
    gpuPrisma = es.toGPUShape(bs.createCylinder(color[0],color[1],color[2],30))

    prisma_scaled = sg.SceneGraphNode("prisma_scaled")
    prisma_scaled.transform = tr.scale(0.5, 0.5, 0.5)
    prisma_scaled.childs += [gpuPrisma]

    prisma_rotated = sg.SceneGraphNode("prisma_rotated_x")
    prisma_rotated.transform = tr.rotationX(0)
    prisma_rotated.childs += [prisma_scaled]

    prisma = sg.SceneGraphNode("prisma")
    prisma.transform = tr.translate(0, 0,0.55)
    prisma.childs += [prisma_rotated]
    return prisma    
def doSomeThings(view,at,up):
    print("view 1 "+str(view))
    print("at 1 "+str(at))
    print("up 1 "+str(up))



if __name__ == "__main__":

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 600
    height = 600

    window = glfw.create_window(width, height, "Tarea 2", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

    # tell GLFW to capture our mouse
    glfw.set_input_mode(window, glfw.CURSOR, glfw.CURSOR_DISABLED);

    # Connecting callback functions to handle mouse events:
    # - Cursor moving over the window
    # - Mouse buttons input
    # - Mouse scroll
    glfw.set_cursor_pos_callback(window, mouse_callback)
    glfw.set_scroll_callback(window, scroll_callback)

    # Assembling the shader program (pipeline) with shaders (simple, texture and lights)
    mvcPipeline = es.SimpleModelViewProjectionShaderProgram()
    colorShaderProgram = es.SimpleModelViewProjectionShaderProgram()
    phongPipeline = ls.SimplePhongShaderProgram()
    TextureShader = es.SimpleTextureModelViewProjectionShaderProgram()

    # Assembling the shader program
    # pipeline = es.SimpleModelViewProjectionShaderProgram()

    # Telling OpenGL to use our shader program
    glUseProgram(mvcPipeline.shaderProgram)

    # Setting up the clear screen color
    glClearColor(0.8, 0.8, 0.8, 1.0)

    # As we work in 3D, we need to check which part is in front,
    # and which one is at the back
    glEnable(GL_DEPTH_TEST)


    #poner archivos transparentes y 
    #glEnable(GL_BLEND)
    #glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    # Creating shapes on GPU memory
    '''
    gpuRedCube = es.toGPUShape(bs.createColorCube(1,0,0))
    gpuGreenCube = es.toGPUShape(bs.createColorCube(0,1,0))
    gpuBlueCube = es.toGPUShape(bs.createColorCube(0,0,1))
    gpuYellowCube = es.toGPUShape(bs.createColorCube(1,1,0))
    gpuCyanCube = es.toGPUShape(bs.createColorCube(0,1,1))
    gpuPurpleCube = es.toGPUShape(bs.createColorCube(1,0,1))
    gpuRainbowCube = es.toGPUShape(bs.createRainbowCube())
    '''
    #gpuAxis = es.toGPUShape(bs.createAxis(7))
    groundNode = createGround("image/suelo.png")
    cielo = createScene("image/gsky.png")
    scenario = createScenario("image/suelo2.png")
    sidetemple = createSideTemple()
    sidepilars = createsidepilars()
    #leftpilars = createleftpilars(3,4)
    #rightside = createrightSide()
    #rightpilars = createrightPilars(3,4)
    centerside = createCenterSide()
    centerpilars = createcenterpilars(3,4)
    ladder = createladder()
    esfera = createEsferas()

    '''#elise
    vertices = [[1, 0], [0.9, 0.4], [0.5, 0.5], [0, 0.5]]
    curve = catrom.getSplineFixed(vertices, 10)
    obj_planeT = bs_ext.createColorPlaneFromCurve(curve, True, 0.3, 0.5, .4)
    obj_planeT.setShader(mvcPipeline)
    obj_planeT.rotationX(np.pi/2)
    obj_planeT.rotationZ(np.pi/2)
    obj_planeT.rotationX(np.pi/7)
    obj_planeT.scale(sx=2.7,sy=2.5,sz=1)
    obj_planeT.translate(tx=10.,ty=9.5,tz=9.7)

    obj_planeC = bs_ext.createColorPlaneFromCurve(curve, False, 0.2, 0.5, 0.4, center=(0, 0))
    obj_planeC.setShader(mvcPipeline)
    obj_planeC.rotationZ(np.pi)
    obj_planeC.scale(sx=2.5,sy=2.5,sz=1)
    obj_planeC.translate(tx=10,ty=10.4,tz=10)

    obj_planeC_2 = bs_ext.createColorPlaneFromCurve(curve, False, 0.2, 0.5, 0.4, center=(0, 0))
    obj_planeC_2.setShader(mvcPipeline)
    obj_planeC_2.rotationX(np.pi)
    obj_planeC_2.scale(sx=2.5,sy=2.5,sz=1)
    obj_planeC_2.translate(tx=10,ty=10.4,tz=10)

    r = 10'''

    normal_view = tr.lookAt(
            controller.cameraPos,
            controller.cameraPos + controller.cameraFront,
            controller.cameraUp
        )
    eye = np.array([5.0, 80.0,  6.0])
    at = np.array([0.0, -1.0, 0.0])
    up = np.array([0.0, 0.0,  1.0])

    normal_view = tr.lookAt(eye,at,up)

    # lookAt of normal camera
    t0 = glfw.get_time()
    v = controller.firstMouse
    valor1 = 2*glfw.get_cursor_pos(window)
    model = tr.identity()
   
 
    
    while not glfw.window_should_close(window):
        glUseProgram(mvcPipeline.shaderProgram)
        valor = glfw.get_cursor_pos(window)
          # Getting the time difference from the previous iteration
        t1 = glfw.get_time()
        dt = t1 - t0
        t0 = t1
        
        if (glfw.get_key(window, glfw.KEY_W) == glfw.PRESS):
            controller.cameraPos += 30*dt * controller.cameraFront

        elif (glfw.get_key(window, glfw.KEY_S) == glfw.PRESS):
            controller.cameraPos -= 30*dt * controller.cameraFront
            #print(controller.cameraPos)


        elif (glfw.get_key(window, glfw.KEY_A) == glfw.PRESS):
            side = np.cross(controller.cameraFront, controller.cameraUp)
            controller.cameraPos -= side / np.linalg.norm(side) * 10 * dt

        elif (glfw.get_key(window, glfw.KEY_D) == glfw.PRESS):
            side = np.cross(controller.cameraFront, controller.cameraUp)
            controller.cameraPos += side / np.linalg.norm(side) * 10 * dt
        #lookAt(eye, at, up)
        view = tr.lookAt(
            controller.cameraPos,
            controller.cameraPos + controller.cameraFront,
            controller.cameraUp
        )
        '''
        Si presionamos las teclas 1,2,3 o 4 podemos cambiar la vista de la camara a una estatica.
        PRECACION: EN LA VISTA ESTATICA, CUALQUIER MOVIMIENTO EN EL MOUSE O AL PRESIONAR LAS TECLAS A,W,S O D
        SE GENERA UN CAMBIO EN LA VISTA ORIGINAL (INICIAL).


        '''
        if(controller.ChangeView):
            view = changeView(controller.POSCAM)
        '''
        print("eye = np.array(" + str(controller.cameraPos))
        print("at = np.array(" + str(controller.cameraFront))
        print("up = np.array(" + str(controller.cameraUp))
        print("controller.lastX = " + str(controller.lastX))
        print("controller.lastY = " + str(controller.lastY))
        print("controller.pitch = " + str(controller.pitch))
        print("controller.fov = " + str(controller.fov))
        print("controller.yaw = " + str(controller.yaw))
        '''

        projection = tr.perspective(math.radians(controller.fov), float(width)/float(height), 1.0, 100)
        glUniformMatrix4fv(glGetUniformLocation(mvcPipeline.shaderProgram, "projection"), 1, GL_TRUE, projection)
        glUniformMatrix4fv(glGetUniformLocation(mvcPipeline.shaderProgram, "view"), 1, GL_TRUE, view)

        # Using GLFW to check for input events
        glfw.poll_events()

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # Filling or not the shapes depending on the controller state
        if (controller.fillPolygon):
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        else:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)


        if controller.record:
            def printit():
                with open('camera.csv', 'w') as csvfile:
                    filewriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    filewriter.writerow([controller.cameraPos,controller.cameraPos+controller.cameraFront,controller.cameraUp])
                    print(glfw.get_time())
            time.sleep(5)
            printit()
            
                

            
            # Assembling the shader program (pipeline) with both shaders
        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        sg.drawSceneGraphNode(sidepilars, mvcPipeline, "model")
        sg.drawSceneGraphNode(centerpilars, mvcPipeline, "model")
        
        glUseProgram(TextureShader.shaderProgram)
        glUniformMatrix4fv(glGetUniformLocation(TextureShader.shaderProgram, "projection"), 1, GL_TRUE, projection)
        glUniformMatrix4fv(glGetUniformLocation(TextureShader.shaderProgram, "view"), 1, GL_TRUE, view)
        glUniformMatrix4fv(glGetUniformLocation(TextureShader.shaderProgram, "model"), 1, GL_TRUE, model)
        
        sg.drawSceneGraphNode(groundNode, TextureShader, "model")
        sg.drawSceneGraphNode(scenario, TextureShader, "model")
        sg.drawSceneGraphNode(cielo, TextureShader, "model")
        sg.drawSceneGraphNode(sidetemple, TextureShader, "model")
        sg.drawSceneGraphNode(centerside, TextureShader, "model")
        sg.drawSceneGraphNode(ladder, TextureShader, "model")
        sg.drawSceneGraphNode(esfera, TextureShader, "model")
        #sg.drawSceneGraphNode(top, TextureShader, "model")
        
        
        

        glUseProgram(phongPipeline.shaderProgram)

        # White light in all components: ambient, diffuse and specular.
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "La"), 1.0, 1.0, 1.0)
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ld"), 1.0, 1.0, 1.0)
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ls"), 1.0, 1.0, 1.0)

        # Object is barely visible at only ambient. Diffuse behavior is slightly red. Sparkles are white
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ka"), 0.2, 0.2, 0.2)
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Kd"), 0.9, 0.5, 0.5)
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ks"), 1.0, 1.0, 1.0)

        # TO DO: Explore different parameter combinations to understand their effect!

        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "lightPosition"), -5, -5, 5 * np.sin(glfw.get_time() / 2))
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "viewPosition"), 5, 5, 6)
        glUniform1ui(glGetUniformLocation(phongPipeline.shaderProgram, "shininess"), 100)
        

        glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "constantAttenuation"), 0.001)
        glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "linearAttenuation"), 0.1)
        glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "quadraticAttenuation"), 0.01)

        glUniformMatrix4fv(glGetUniformLocation(phongPipeline.shaderProgram, "projection"), 1, GL_TRUE, projection)
        glUniformMatrix4fv(glGetUniformLocation(phongPipeline.shaderProgram, "view"), 1, GL_TRUE, normal_view)
        glUniformMatrix4fv(glGetUniformLocation(phongPipeline.shaderProgram, "model"), 1, GL_TRUE, model)
        
        
        
        
       
     
        
       
        #sg.drawSceneGraphNode(cilinder, TextureShader, "model")
        # Once the drawing is rendered, buffers are swap so an uncomplete drawing is never seen.
        glfw.swap_buffers(window)

    glfw.terminate()